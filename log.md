# ITT3-PCB-Sebastian
## Week 43
### Tuesday
* Heard presentation from Nordcad about OrCad
* Did exercises from Nordcad 

### Wednesday
* Exams

## Week 44
### Tuesday
* Began designing wireless power receiver based on datasheet
* Calculated values of components for schematic

### Wednesday
* Finished initial schematic
* Begin on PCB layout and design
* Found schematic and PCB for wireless power transmitter

## Week 45
### Tuesday
* Finished schematic for wireless power receiver
* Finished PCB for wireless power receiver

### Wednesday
* Double checked both designs
* Added BOM to receiver design
* Added missing parts to both BOM's

## Week 46
### To do
1. Add solder mask to AC1 and AC2 holes
1. Figure out how to upload multiple designs or how to panelize myself


### Tuesday
* Changed the resistor sizes on the receiver PCB to match availability
* Shortened the transmitter PCB to fit more on a board
* Exported Gerber files for both PCBs

### Wednesday
* Helped other people finish their PCB designs
* Added solder mask to receiver coil board hole

## Week 47
### Tuesday
* Listened to presentation about "design for manufacturing"
* Had to leave early because of internship interview

### Wednesday
* Finished Gerber files for transmitter
* Finished Gerber files for receiver
* Finished BOM file for transmitter
* Finished BOM file for receiver

## Week 48
### Tuesday
* Listened to presentation about "advanced PCB design"
* Ordered components

### Wednesday
* Finished spreadsheet for PCB manufacturing cost

## Week 49
### Tuesday
* Listened to presentation about how the exam will be held
* Helped other people finish their PCBs
* Begun work on poster presentation
* Waiting for components to arrive


### Wednesday
* Continued working on poster presentation
* Waiting for components to arrive
